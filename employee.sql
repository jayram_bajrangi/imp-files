CREATE TABLE `BeauticianWorkTracker` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GUID` varchar(50) NOT NULL,
  `BeauticianID` int(11) NOT NULL,
  `Notes` varchar(150) DEFAULT NULL,
  `WorkTrackerType` enum('LEAVE','WEEKOFFWORK') DEFAULT NULL,
  `InformedOnDate` varchar(13) DEFAULT NULL,
  `FromDate` varchar(13) DEFAULT NULL,
  `FromHalf` bit(1) DEFAULT b'0',
  `ToDate` varchar(13) DEFAULT NULL,
  `ToHalf` bit(1) DEFAULT b'1',
  `insertTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GUID_UNIQUE` (`GUID`),
  KEY `fkey-worktracker-beautician_idx` (`BeauticianID`),
  CONSTRAINT `fkey-worktracker-beautician` FOREIGN KEY (`BeauticianID`) REFERENCES `Beautician` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `BeauticianAccountTransactionType` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` varchar(5) NOT NULL,
  `Description` varchar(50) NOT NULL,
  `IsCredit` bit(1) NOT NULL DEFAULT b'0',
  `insertTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `BeauticianAccountTransaction` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GUID` varchar(50) DEFAULT NULL,
  `Date` varchar(13) NOT NULL,
  `BeauticianID` int(11) NOT NULL,
  `DebitAmount` double DEFAULT NULL,
  `CreditAmount` double DEFAULT NULL,
  `Notes` varchar(150) DEFAULT NULL,
  `TransactionTypeID` bigint(20) NOT NULL,
  `WorkTrackerID` bigint(20) DEFAULT NULL,
  `WorkType` enum('FULLDAY','HALFDAY','OTHER') DEFAULT NULL,
  `IsCancelled` bit(1) DEFAULT b'0',
  `insertTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GUID_UNIQUE` (`GUID`),
  KEY `fkey-transaction-beautician_idx` (`BeauticianID`),
  KEY `fkey-transaction-type_idx` (`TransactionTypeID`),
  KEY `fkey-transaction-work-tracker_idx` (`WorkTrackerID`),
  CONSTRAINT `fkey-transaction-beautician` FOREIGN KEY (`BeauticianID`) REFERENCES `Beautician` (`ID`),
  CONSTRAINT `fkey-transaction-type` FOREIGN KEY (`TransactionTypeID`) REFERENCES `BeauticianAccountTransactionType` (`ID`),
  CONSTRAINT `fkey-transaction-work-tracker` FOREIGN KEY (`WorkTrackerID`) REFERENCES `BeauticianWorkTracker` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Add new entity model
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BB0A1C11051664', 'BeauticianAccountTransactionType', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BC0A1C11051664', 'BeauticianWorkTracker', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BD0A1C11051664', 'BeauticianAccountTransaction', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BF0A1C11051664', 'BeauticianAccountTransaction', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BG0A1C11051664', 'BeauticianAccountTransaction', 'Delete');

-- populate BeauticianAccountTransactionType
INSERT INTO `lookplexdb`.`BeauticianAccountTransactionType` (`Type`, `Description`, `IsCredit`) VALUES ('LD', 'Leave Deduction', b'0');
INSERT INTO `lookplexdb`.`BeauticianAccountTransactionType` (`Type`, `Description`, `IsCredit`) VALUES ('RP', 'Rejection Penalty', b'0');
INSERT INTO `lookplexdb`.`BeauticianAccountTransactionType` (`Type`, `Description`, `IsCredit`) VALUES ('WC', 'WeekOff Compensation', b'1');