CREATE TABLE `lookplexdb`.`Book` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL DEFAULT 'India',
  `Description` VARCHAR(150) NULL,
  `Currency` ENUM('INR', 'USD', 'BITCOIN') NOT NULL DEFAULT 'INR',
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC));
  
CREATE TABLE `lookplexdb`.`Account` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(500) NULL DEFAULT NULL,
  `Code` VARCHAR(45) NULL DEFAULT NULL,
  `ParentAccountID` BIGINT(20) NULL DEFAULT NULL,
  `AccountNumber` VARCHAR(45) NULL DEFAULT NULL,
  `NormalBalance` ENUM('DEBIT','CREDIT') NOT NULL,
  `BookID` BIGINT(20) NOT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC),
  UNIQUE INDEX `uq-name-book` (`Name` ASC,`BookID` ASC),
  INDEX `fkey-account-book_idx` (`BookID` ASC),
  CONSTRAINT `fkey-account-book` 
    FOREIGN KEY (`BookID`) 
    REFERENCES `lookplexdb`.`Book` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

    
CREATE TABLE `lookplexdb`.`JournalEntry` (
  `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `Basis` ENUM('CASH', 'ACCRUAL', 'BOTH') NOT NULL DEFAULT 'BOTH',
  `EffectiveAt` VARCHAR(13) NOT NULL,
  `ActiveCityID` BIGINT(20) NULL,
  `Description` VARCHAR(150) NULL DEFAULT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC),
  INDEX `fkey-journal-city_idx` (`ActiveCityID` ASC),
  CONSTRAINT `fkey-journal-city`
    FOREIGN KEY (`ActiveCityID`)
    REFERENCES `lookplexdb`.`ActiveCity` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `lookplexdb`.`Transaction` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `Description` VARCHAR(150) NULL DEFAULT NULL,
  `DebitAmount` BIGINT NOT NULL DEFAULT 0,
  `CreditAmount` BIGINT NOT NULL DEFAULT 0,
  `JournalID` BIGINT NOT NULL,
  `AccountID` BIGINT NOT NULL,
  `ReferenceType` VARCHAR(45) NULL DEFAULT NULL,
  `ReferenceID` VARCHAR(50) NULL DEFAULT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC),
  INDEX `fkey-tnx-journal_idx` (`JournalID` ASC),
  INDEX `fkey-tnx-account_idx` (`AccountID` ASC),
  CONSTRAINT `fkey-tnx-journal`
    FOREIGN KEY (`JournalID`)
    REFERENCES `lookplexdb`.`JournalEntry` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkey-tnx-account`
    FOREIGN KEY (`AccountID`)
    REFERENCES `lookplexdb`.`Account` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `lookplexdb`.`Report` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(150) NULL DEFAULT NULL,
  `BookID` BIGINT NOT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC),
  INDEX `fkey-report-book_idx` (`BookID` ASC),
  UNIQUE INDEX `uq-name-book` (`Name` ASC, `BookID` ASC),
  CONSTRAINT `fkey-report-book`
    FOREIGN KEY (`BookID`)
    REFERENCES `lookplexdb`.`Book` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
CREATE TABLE `lookplexdb`.`Category` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `ParentCategoryID` BIGINT NULL DEFAULT NULL,
  `NormalBalance` ENUM('DEBIT', 'CREDIT') NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(150) NULL DEFAULT NULL,
  `BookID` BIGINT NOT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC),
  INDEX `fkey-category-book_idx` (`BookID` ASC),
  UNIQUE INDEX `uq-name-book` (`Name` ASC, `BookID` ASC),
  CONSTRAINT `fkey-category-book`
    FOREIGN KEY (`BookID`)
    REFERENCES `lookplexdb`.`Book` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
ALTER TABLE `lookplexdb`.`Category` 
ADD INDEX `fkey-category_idx` (`ParentCategoryID` ASC);
;
ALTER TABLE `lookplexdb`.`Category` 
ADD CONSTRAINT `fkey-category`
  FOREIGN KEY (`ParentCategoryID`)
  REFERENCES `lookplexdb`.`Category` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `lookplexdb`.`Account` 
ADD INDEX `fkey-account_idx` (`ParentAccountID` ASC);
;
ALTER TABLE `lookplexdb`.`Account` 
ADD CONSTRAINT `fkey-account`
  FOREIGN KEY (`ParentAccountID`)
  REFERENCES `lookplexdb`.`Account` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
CREATE TABLE `lookplexdb`.`CategoryReport` (
  `CategoryID` BIGINT NOT NULL,
  `ReportID` BIGINT NOT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CategoryID`, `ReportID`),
  INDEX `fkey-cr-report_idx` (`ReportID` ASC),
  CONSTRAINT `fkey-cr-category`
    FOREIGN KEY (`CategoryID`)
    REFERENCES `lookplexdb`.`Category` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkey-cr-report`
    FOREIGN KEY (`ReportID`)
    REFERENCES `lookplexdb`.`Report` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
CREATE TABLE `lookplexdb`.`AccountCategory` (
  `AccountID` BIGINT NOT NULL,
  `CategoryID` BIGINT NOT NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AccountID`, `CategoryID`),
  INDEX `fkey-ac-category_idx` (`CategoryID` ASC),
  CONSTRAINT `fkey-ac-account`
    FOREIGN KEY (`AccountID`)
    REFERENCES `lookplexdb`.`Account` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkey-ac-category`
    FOREIGN KEY (`CategoryID`)
    REFERENCES `lookplexdb`.`Category` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
-- 

INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BH0A1C11051664', 'Book', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BI0A1C11051664', 'Book', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BJ0A1C11051664', 'Account', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BK0A1C11051664', 'Account', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BL0A1C11051664', 'Transaction', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BM0A1C11051664', 'Transaction', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BN0A1C11051664', 'Journal', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BO0A1C11051664', 'Journal', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BP0A1C11051664', 'Category', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BQ0A1C11051664', 'Category', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BR0A1C11051664', 'Report', 'Read');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BS0A1C11051664', 'Report', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BT0A1C11051664', 'Transaction', 'Delete');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BU0A1C11051664', 'Journal', 'Update');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BV0A1C11051664', 'Journal', 'Delete');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BW0A1C11051664', 'Tag', 'Create');
INSERT INTO `lookplexdb`.`EntityModel` (`GUID`, `EntityName`, `Permission`) VALUES ('157899C1B911E989BX0A1C11051664', 'Tag', 'Read');
-- 
ALTER TABLE `lookplexdb`.`Book` 
ADD COLUMN `GUID` VARCHAR(50) NOT NULL AFTER `ID`,
DROP INDEX `Name_UNIQUE` ;
ALTER TABLE `lookplexdb`.`Book` 
ADD UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC);
-- 
CREATE TABLE `lookplexdb`.`Tag` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `HasValue` BIT(1) NULL DEFAULT 0,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC));
  
CREATE TABLE `lookplexdb`.`JournalTag` (
  `ID` BIGINT NOT NULL AUTO_INCREMENT,
  `GUID` VARCHAR(50) NOT NULL,
  `TagID` BIGINT NOT NULL,
  `JournalID` BIGINT NOT NULL,
  `TagValue` VARCHAR(50) NULL,
  `insertTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `GUID_UNIQUE` (`GUID` ASC),
  INDEX `fkey-journaltag-journal_idx` (`JournalID` ASC),
  INDEX `fkey-journaltag-tag_idx` (`TagID` ASC),
  CONSTRAINT `fkey-journaltag-journal`
    FOREIGN KEY (`JournalID`)
    REFERENCES `lookplexdb`.`JournalEntry` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fkey-journaltag-tag`
    FOREIGN KEY (`TagID`)
    REFERENCES `lookplexdb`.`Tag` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
-- 
ALTER TABLE `lookplexdb`.`Tag` 
ADD UNIQUE INDEX `Name_UNIQUE` (`Name` ASC);
-- Book and Categories
INSERT INTO `lookplexdb`.`Book` (`GUID`, `Name`, `Currency`) VALUES ('A264032CF5F745659EF5EF5DADAC4EF7', 'India', 'INR');
-- Tags
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('761A46B8C249493E8ED721E3E6F251E7', 'Expense', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('9FFD226DE2B540459B0C11C32AAD3B56', 'InvoiceNumber', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('C799CBB7D85A442BBF3CAD371791126E', 'BookingID', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('70768A6ADE8C4FD79A65974159624582', 'CustomerID', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('9AF677F4E893455C80C34D98B3819337', 'InvoiceCreatedOrUpdated', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('E17C4AFB0B604828A7B9D3C9CA41E48F', 'InvoicePaid', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('DE19AFCA41DA493AA305E5A31E378078', 'DueDate', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('3F625A1630A447F5985825E8B66FB48E', 'BeauticianID', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('EE19AFCA41DA493AA305E5A31E378078', 'PaymentMode', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('FE19AFCA41DA493AA305E5A31E378078', 'PaymentReconciled', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('GE19AFCA41DA493AA305E5A31E378078', 'CashCollectedFromBeautician', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('HE19AFCA41DA493AA305E5A31E378078', 'CustomerPayment', 1);
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('IE19AFCA41DA493AA305E5A31E378078', 'PaymentRefund', 1);
-- Vendor
CREATE TABLE `Vendor` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GUID` varchar(50) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Company` varchar(45) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `insertTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTS` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GUID_UNIQUE` (`GUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `lookplexdb`.`Tag` (`GUID`, `Name`, `HasValue`) VALUES ('JE19AFCA41DA493AA305E5A31E378078', 'VendorID', 1);