-- data sync template
insert into BookingTrack (GUID, CustomerName, CustomerPhoneNumber, PayAmount, ServeDateTime, ZohoInvoiceID, CustomerAddress, Beautician, InternalNote, isCancelled)
select GUID, CustomerName, Phone, Amount, 
str_to_date(concat(ServingDate, ' ', replace(replace(ServingTime, ' PM', ':00 PM'), ' AM', ':00 AM')), '%Y-%m-%d %r'), 
ZohoInvoiceID, City, Beautician, WorkFlowStatus, 1 from tmp.voidedinvoice where GUID not in (select GUID from BookingTrack);

-- feedback
select count(*) from BookingTrack bt join tmp.bookingsurvey bs on bs.text = bt.GUID;
update BookingTrack set isFeedbackTaken = 1 where GUID in (select text from tmp.bookingsurvey); 
# "Question ID" = 294438000000307001 from "Responses (Zoho Survey)"
select Text from tmp.bookingsurvey where Text not in (select GUID from BookingTrack);

-- 
select count(*) from BookingTrack where isPaid = 1 and ZohoInvoiceID is null ;
select count(*) from BookingTrack where isCancelled = 1 and ZohoInvoiceID is null; 
select count(*) from BookingTrack where isFeedbackTaken = 1 and ZohoInvoiceID is null ;

-- IsPaid sync
select count(distinct GUID) from tmp.paidinvoices pi where pi.GUID not in (select GUID from BookingTrack);

update tmp.paidinvoices set `Invoice ID` = reverse(`Invoice ID`);
select count(distinct GUID) from tmp.paidinvoices pi where reverse(pi.`Invoice ID`) not in (select ZohoInvoiceID from BookingTrack);
select count(distinct GUID) from BookingTrack where reverse(ZohoInvoiceID) not in (select `Invoice ID` from tmp.paidinvoices) and IsPaid = 1;
select bt.GUID from BookingTrack bt join tmp.paidinvoices pi on reverse(ZohoInvoiceID) = pi.`Invoice ID` where bt.IsPaid = 1 and 
bt.GUID != pi.GUID;
select count(*) from BookingTrack bt join tmp.paidinvoices pi on reverse(ZohoInvoiceID) = pi.`Invoice ID` where bt.IsPaid = 1 and 
bt.GUID = pi.GUID;
select group_concat(pi.GUID) from BookingTrack bt join tmp.paidinvoices pi on reverse(ZohoInvoiceID) = pi.`Invoice ID` where bt.IsPaid = 1 and bt.GUID != pi.GUID;
update BookingTrack bt set GUID = 
(select GUID from tmp.paidinvoices pi where pi.`Invoice ID`=reverse(bt.ZohoInvoiceID) and pi.GUID != bt.GUID) 
where isPaid = 1 and GUID in ('3QHU76WV', 'TDR35FV4', 'TL3MZZFY');

insert into BookingTrack (GUID, CustomerName, CustomerPhoneNumber, PayAmount, PaidAmount, ServeDateTime, ZohoInvoiceID, CustomerAddress, isPaid, Beautician) select GUID, CustomerName, Phone, replace(replace(`Total (BCY)`, 'INR ', ''), ',', ''), replace(replace(`Total (BCY)`, 'INR ', ''), ',', ''), 
str_to_date(concat(ServingDate, ' ', replace(replace(ServingTime, ' PM', ':00 PM'), ' AM', ':00 AM')), '%d/%m/%Y %r'), 
`Invoice ID`, City, 1, Beautician from tmp.paidinvoices where GUID not in (select GUID from BookingTrack);

update BookingTrack set CustomerID = (select ID from Customer where Phone = CustomerPhoneNumber) where CustomerID is null;

update BookingTrack set ActiveCityID = 
(select ID from ActiveCity where CityName = CustomerAddress or find_in_set(CustomerAddress, Alias) > 0) 
where ActiveCityID is null;

update BookingTrack set BeauticianID = 
(select ID from Beautician where Phone = SUBSTRING_INDEX(Beautician, '~', -1)) 
where BeauticianID is null and (Beautician is not null and Beautician != '');

select count(distinct GUID) from tmp.paidinvoices pi where reverse(pi.`Invoice ID`) not in (select ZohoInvoiceID from BookingTrack);
select count(*) from BookingTrack bt join tmp.paidinvoices pi on reverse(ZohoInvoiceID) = pi.`Invoice ID` where bt.IsPaid =1 and 
bt.GUID != pi.GUID;
select GUID from tmp.paidinvoices pi where pi.GUID not in (select GUID from BookingTrack where IsPaid = 1);
-- 
show processlist;
kill 18729;
-- Populate feedback table
update tmp.zohosurvey set `Respondent ID` = reverse(`Respondent ID`);
update tmp.zohosurvey set `Question ID` = reverse(`Question ID`);

ALTER TABLE `tmp`.`zohosurvey` 
ADD INDEX `idx_respondant` (`Respondent ID` ASC),
ADD INDEX `idx_question` (`Question ID` ASC);

select count(*) from BookingTrack bt join tmp.zohosurvey zs on bt.GUID = zs.Text where zs.`Question ID` = reverse(294438000000307001);
select count(*) from tmp.zohosurvey where `Question ID` = reverse(294438000000307001);

insert into Feedback(BookingID, CustomerID, BeauticianID, FeedbackBy) 
	select ID, CustomerID, BeauticianID, 1 
        from BookingTrack bt join tmp.zohosurvey zs on bt.GUID = zs.Text 
		where zs.`Question ID` = reverse(294438000000307001);

select count(*) from Feedback where BeauticianID = 0;

update Feedback f set Hygiene = 
	(select Text from tmp.zohosurvey where `Question ID` = reverse(294438000000007095)
		and `Respondent ID` = (select `Respondent ID` from tmp.zohosurvey zs join BookingTrack bt on bt.GUID = zs.Text 
		where zs.`Question ID` = reverse(294438000000307001) and bt.ID = f.BookingID)
	);

update Feedback f set Behaviour = 
	(select Text from tmp.zohosurvey where `Question ID` = reverse(294438000000007107)
		and `Respondent ID` = (select `Respondent ID` from tmp.zohosurvey zs join BookingTrack bt on bt.GUID = zs.Text 
		where zs.`Question ID` = reverse(294438000000307001) and bt.ID = f.BookingID)
	);
    
update Feedback f set Overall = 
	(select Text from tmp.zohosurvey where `Question ID` = reverse(294438000000007119)
		and `Respondent ID` = (select `Respondent ID` from tmp.zohosurvey zs join BookingTrack bt on bt.GUID = zs.Text 
		where zs.`Question ID` = reverse(294438000000307001) and bt.ID = f.BookingID)
	);

update Feedback f set Comment = 
	(select Text from tmp.zohosurvey where `Question ID` = reverse(294438000000007157)
		and `Respondent ID` = (select `Respondent ID` from tmp.zohosurvey zs join BookingTrack bt on bt.GUID = zs.Text 
		where zs.`Question ID` = reverse(294438000000307001) and bt.ID = f.BookingID)
	);