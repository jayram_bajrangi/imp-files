/*
From zoho analytics,
Columns to be downloaded: ID,Entity Type,Entity ID,Transaction Number,Transaction Amount (BCY),Account ID,Debit or Credit,Transaction Date,Transaction ID,Reference No.
upload the csv files in local dbtables CashTransaction and AccrualTransaction.
export the self-contained sql files in dump folder from localdb . (only data and not schema)
import sql files in lookplex db (select the schema before importing)
Download Accounts from analytics
*/
/*
If count in zohoaccount table is not equal to the count of accounts downloaded from ananlytics then run following script
*/
delete from ZohoAccount;
/*
AND also 
1. import the accounts in ZohoAccount table.
2. populate tables accountCategory, account, category from zohoaccount table using the script BELOW.
*/
select ID into @BookID from Book where Name = 'India';
Insert ignore into Account (GUID, Name, Code, BookID) 
	select ID, Name, Code, @BookID from ZohoAccount;
Insert ignore into Category (GUID, Name, NormalBalance, BookID) 
	select max(ID), Type, 'DEBIT', @BookID from ZohoAccount group by Type;
insert ignore Into AccountCategory(AccountID, CategoryID)
	select a.ID, c.ID from Account a 
		join ZohoAccount za on za.ID = a.GUID 
		join Category c on c.Name = za.Type;
-- 
/*
Check the count of cash and accrual transactions with analytics. delete all transactions and journal entries after the max matching date. Change the table names of cashTransa and accrualTransa and create these tables again. And upload the fresh data with the id in analytics greater then the GUID of transactions in lookplex after removing C and A separately;
*/
/*CASH: Last time I checked data before 01/07/2019 was matching with analytics*/
/*ACCRUAL: Last time I checked data before 01/06/2019 was matching with analytics*/
/*
delete from Transaction where ID in (
	select t.ID from Transaction t join JournalEntry je on je.ID = t.JournalID
    where je.EffectiveAt >= UNIX_TIMESTAMP('2019-07-01')*1000 and Basis = 'CASH'
)

delete from Transaction where ID in (
	select t.ID from Transaction t join JournalEntry je on je.ID = t.JournalID
    where je.EffectiveAt >= UNIX_TIMESTAMP('2019-06-01')*1000 and Basis = 'ACCRUAL'
)
*/
#select max(ID) into @MAX_JOURNAL_ENTRY_GUID from JournalEntry
insert ignore into JournalEntry (GUID, Basis, EffectiveAt, EntityType, EntityID) 
	select concat('C', JournalID), 'CASH', max(UNIX_TIMESTAMP(STR_TO_DATE(t.Date, '%d/%m/%Y'))*1000),  
		max(EntityType), max(EntityID)
	from CashTransaction t
		-- where STR_TO_DATE(t.Date, '%d/%m/%Y') < '2020-01-01'  
		-- and STR_TO_DATE(t.Date, '%d/%m/%Y') >= '2017-01-01' 
        group by JournalID;
#select max(ID) into @MAX_JOURNAL_ENTRY_GUID2 from JournalEntry
insert ignore into JournalEntry (GUID, Basis, EffectiveAt, EntityType, EntityID) 
	select concat('A', JournalID), 'ACCRUAL', max(UNIX_TIMESTAMP(STR_TO_DATE(t.Date, '%d/%m/%Y'))*1000),  
		max(EntityType), max(EntityID)
	from AccrualTransaction t
		-- where STR_TO_DATE(t.Date, '%d/%m/%Y') < '2020-01-01'  
		-- and STR_TO_DATE(t.Date, '%d/%m/%Y') >= '2017-01-01' 
        group by JournalID;
-- 
insert ignore into Transaction (GUID, DebitAmount, CreditAmount, JournalID, AccountID) 
	select concat('C', ct.ID), 
		(case ct.NormalBalance when 'debit' then REPLACE(REPLACE(REPLACE(Amount, 'INR ', ''), ',', ''), '.', '') else 0 end), 
        (case ct.NormalBalance when 'credit' then REPLACE(REPLACE(REPLACE(Amount, 'INR ', ''), ',', ''), '.', '') else 0 end), 
        je.ID, a.ID from CashTransaction ct 
		join JournalEntry je on je.GUID = concat('C', ct.JournalID) 
        join Account a on a.GUID = ct.AccountID; #where je.ID > @MAX_JOURNAL_ENTRY_GUID and je.ID <= @MAX_JOURNAL_ENTRY_GUID2

insert ignore into Transaction (GUID, DebitAmount, CreditAmount, JournalID, AccountID) 
	select concat('A', ct.ID), 
		(case ct.NormalBalance when 'debit' then REPLACE(REPLACE(REPLACE(Amount, 'INR ', ''), ',', ''), '.', '') else 0 end), 
        (case ct.NormalBalance when 'credit' then REPLACE(REPLACE(REPLACE(Amount, 'INR ', ''), ',', ''), '.', '') else 0 end), 
        je.ID, a.ID from AccrualTransaction ct 
		join JournalEntry je on je.GUID = concat('A', ct.JournalID) 
        join Account a on a.GUID = ct.AccountID; #where je.ID > @MAX_JOURNAL_ENTRY_GUID2
-- 
update BookingTrack set ZohoInvoiceID = null where ZohoInvoiceID like 'INVOICE-%';
-- JOINS are costly and takes time, so be patient and keep checking the status using "show processlist", don't run it twice.
-- RUN each update or insert one by one
update JournalEntry 
	set ActiveCityID = (select max(bt.ActiveCityID) from BookingTrack bt 
		where ZohoInvoiceID = EntityID) 
    where EntityType = 'invoice' and ActiveCityID is null;
-- 
insert ignore PhoneCity (Phone, ActiveCityID) 
	select c.Phone, ca.ActiveCityID from Customer c 
		join CustomerAddress ca on ca.CustomerID = c.ID;
update paymentphone pp set ActiveCityID = (select max(pc.ActiveCityID) from PhoneCity pc 
	where pc.Phone = pp.Phone) where ActiveCityID is null; 
update JournalEntry je set ActiveCityID = (select max(pp.ActiveCityID) from paymentphone pp 
	where pp.PaymentID = je.EntityID) where ActiveCityID is null and EntityType = 'customer_payment';
-- 
update JournalEntry set ActiveCityID = (select max(ActiveCityID) from BookingTrack bt    
				join invoicepayment ip on ip.InvoiceID = cast(bt.ZohoInvoiceID as char(20))
                where ip.ID = EntityID) where EntityType = 'invoice_payment' and ActiveCityID is null;